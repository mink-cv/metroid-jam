# MetroidJam Game

A submission for the #metroidjam by [@minkcv](twitter.com/minkcv) and [@beatscribe](twitter.com/beatscribe)

Follow the game to get notifications about updates and fixes. (I won't spam you)

*Controls*
Arrows: Move and aim
Z: Jump
X: Shoot
Left Shift: Toggle missiles
